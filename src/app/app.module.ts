import { HttpClient, HttpClientModule, HttpHandler, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SearchBoxComponent } from './search-box/search-box.component';
import { ConfigService } from './service/config.service';
import { HttpService } from './service/http.service';
import { CookieService } from 'ngx-cookie-service';
import { Newworld } from './search-result/search-newworld';
import { Countdown } from './search-result/search-countdown';
import { PaknSave } from './search-result/search-paknsave';
import { EmailService } from './service/email-service';

@NgModule({
  declarations: [
    AppComponent,
    SearchBoxComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
  ],
  providers: [
    { provide: HttpService },
    { provide: ConfigService },
    { provide: CookieService },
    { provide: Newworld },
    { provide: Countdown },
    { provide: PaknSave },
    { provide: HttpClient },
    { provide: EmailService },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
