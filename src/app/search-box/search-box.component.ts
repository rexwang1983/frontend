import { Component, ElementRef, Injectable, OnInit, ViewChild } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Newworld } from "../search-result/search-newworld"
import { Countdown } from '../search-result/search-countdown';
import { PaknSave } from '../search-result/search-paknsave';
import { EmailService, MailRequest } from '../service/email-service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-search-box',
  templateUrl: './search-box.component.html',
  styleUrls: ['./search-box.component.less']
})

@Injectable()
export class SearchBoxComponent implements OnInit {
  @ViewChild('searchBox')
  searchElement!: ElementRef;
  @ViewChild('details')
  detailsElement!: ElementRef;
  isSearchCountdown = true;
  isSearchNewworld = true;
  isSearchPaknSave = true;
  keyword = "";
  myList: Array<any> | undefined;
  hotkey: any;

  constructor(private cookieService: CookieService,
    public newworld: Newworld,
    public countdown: Countdown,
    public paknSave: PaknSave,
    public emaiService: EmailService,
    public httpClient: HttpClient) {
  }

  ngOnInit(): void {
    this.httpClient.get('../../assets/hotkey.json').subscribe(data => {
      this.hotkey = data;
    });

    var myListCookie = this.cookieService.get('myList');
    if (myListCookie != '' && myListCookie != undefined && myListCookie != null) {
      this.myList = JSON.parse(myListCookie);
    }

    var myNewWorldStore = this.cookieService.get('myNewWorldStore');
    if(myNewWorldStore != '' && myNewWorldStore != undefined && myNewWorldStore != null) {
      this.newworld._selectedStoreName = myNewWorldStore;
    }

    var myPaknSaveStore = this.cookieService.get('myPaknSaveStore');
    if(myPaknSaveStore != '' && myPaknSaveStore != undefined && myPaknSaveStore != null) {
      this.paknSave._selectedStoreName = myPaknSaveStore;
    }
  }

  changeNewWorldStore() {
    this.cookieService.set("myNewWorldStore", this.newworld._selectedStoreName);
    this.newworld.setStore();
  }

  changePaknSaveStore() {
    this.cookieService.set("myPaknSaveStore", this.paknSave._selectedStoreName);
    this.paknSave.setStore();
  }

  public share(socialMedia: string) {
    switch (socialMedia) {
      case "pinterest":
        window.open('http://pinterest.com/pin/create/button/?url=https%3A%2F%2Fbuysomeeasy.azurewebsites.net%2F&description=' + "Buy Something");
        break;
      case "facebook":
        window.open('https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fbuysomeeasy.azurewebsites.net%2F&t=' + "Buy Something");
        break;
      case "twitter":
        window.open('https://twitter.com/intent/tweet?text=%20Check%20up%20this%20awesome%20content' + "Buy Something" + ':%20 https%3A%2F%2Fbuysomeeasy.azurewebsites.net%2F');
        break;
      case "linkedin":
        window.open('http://www.linkedin.com/shareArticle?mini=true&url=https%3A%2F%2Fbuysomeeasy.azurewebsites.net%2F&title=' + "Buy Something");
        break;
    }
  }

  public sendEmail(event: any) {
    var mailRequest = new MailRequest();
    mailRequest.ToEmail = event.target.email.value;
    mailRequest.Subject = "Your shopping list";
    mailRequest.Body = this.detailsElement.nativeElement.innerHTML;
    this.emaiService.send(mailRequest);
  }

  public search() {
    this.searchElement.nativeElement.focus();
    this.countdown._result = [];
    this.newworld._result = [];
    this.paknSave._result = [];
    if (this.isSearchNewworld) {
      console.log("Search new world products");
      this.newworld.search(this.keyword);
    }
    if (this.isSearchCountdown) {
      console.log("Search countdown products");
      this.countdown.search(this.keyword);
    }
    if (this.isSearchPaknSave) {
      console.log("Search Pakn save products");
      this.paknSave.search(this.keyword);
    }
  }

  public addToMyList(store: string, product: any) {
    if (this.myList == undefined) {
      this.myList = [];
    }
    var isNew = true;
    this.myList.forEach(l => {
      if (l.store == store && l.productID == product.productID) {
        l.number++;
        isNew = false;
      }
    });
    if (isNew) {
      this.myList?.push(
        {
          'store': store, 'linkUrl': product.linkUrl,
          'title': product.title, 'price': product.price,
          'productID': product.productID, 'number': 1,
          'image': product.image
        }
      );
    }
    this.cookieService.set('myList', JSON.stringify(this.myList));
  }

  public deleteAllMyList() {
    this.myList = undefined;
    this.cookieService.set('myList', JSON.stringify(this.myList));
  }

  public deleteListProduct(product: any) {
    if (this.myList == undefined)
      return;

    const index: number = this.myList.indexOf(product);
    if (index !== -1) {
      this.myList.splice(index, 1);
    }
    this.cookieService.set('myList', JSON.stringify(this.myList));
  }

  public getStoreList(store: string) {
    if (this.myList == undefined)
      return;

    return this.myList.filter(list => list.store == store);
  }

  public getStoreTotalPrice(store: string) {
    if (this.myList == undefined)
      return;

    var list = this.myList.filter(list => list.store == store);
    var totalPrice = 0;
    list.forEach(p => {
      totalPrice += p.price * p.number;
    });
    return totalPrice.toFixed(2);
  }

  public getTotalPrice() {
    if (this.myList == undefined)
      return;

    var totalPrice = 0;
    this.myList.forEach(p => {
      totalPrice += p.price * p.number;
    });
    return totalPrice.toFixed(2);
  }
}
