import { Injectable } from '@angular/core';
import { HttpService } from '../service/http.service';

@Injectable()
export class Countdown{

  public _result: any;
  public _isSearching = false;

  constructor(private httpService: HttpService) {
  }

  public search(keyword: string): void {
    this._isSearching = true;
    this.httpService.post("countdown", "?keyword=" + keyword)
      .pipe().subscribe((result: any) => {
        this._isSearching = false;
        this._result = result;
      },
        error => {
          this._isSearching = false;
          console.log(error);
        });
    return;
  }
}
