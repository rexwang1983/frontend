import { Injectable } from '@angular/core';
import { HttpService } from '../service/http.service';

@Injectable()
export class Newworld {

  public _result: any;
  public _isSearching = false;
  public _stores: any;
  public _selectedStoreName!: string;
  public _selectedStoreId!: string;

  constructor(private httpService: HttpService) {
    this.getStores();
  }

  public getStores() {
    this.httpService.postExternalLink("https://www.newworld.co.nz/CommonApi/Store/GetStoreList")
      .pipe().subscribe((result: any) => {
        this._stores = result.stores;
        this.setStore();
      },
        error => {
          console.log(error);
        });
    return;
  }

  public setStore() {
    var store = this._stores.find((_s: { name: any; }) => _s.name == this._selectedStoreName);
    if (store != undefined) {
      this.httpService.get("https://www.newworld.co.nz/CommonApi/Store/ChangeStore?storeId=" + store.id + "&clickSource=list")
        .pipe().subscribe((result: any) => {
          this._selectedStoreId = store.id;
          this.httpService.get("https://www.newworld.co.nz/CommonApi/Cart/Index")
            .pipe().subscribe((result: any) => {
            },
              error => {
                console.log(error);
              });
        },
          error => {
            console.log(error);
          });
    }
    return;
  }

  public search(keyword: string): void {
    this._isSearching = true;
    this.httpService.getExternalLink("https://www.newworld.co.nz/shop/Search?q=" + keyword)
      .pipe().subscribe((html: any) => {
        //send html to the back end
        this.httpService.postWithBody("newworld", html)
          .pipe().subscribe((result: any) => {
            this._isSearching = false;
            this._result = result;
          },
            error => {
              this._isSearching = false;
              console.log(error);
            });
      },
        error => {
          this._isSearching = false;
          console.log(error);
        });
    return;
  }
}
