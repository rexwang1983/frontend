import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

@Injectable()
export class ConfigService {
    constructor() {
    }

    public getApiUrl(serviceName: string): string {
        return environment.url + serviceName;
    }

    public getBaseUrl(): string {
        return environment.url;
    }
}