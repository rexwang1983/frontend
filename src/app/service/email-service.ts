import { Injectable } from '@angular/core';
import { HttpService } from '../service/http.service';

@Injectable()
export class EmailService {
  public _isWaitting = false;
  public _message = "";

  constructor(private httpService: HttpService) {
  }

  public send(body: any): boolean {
    this._isWaitting = true;
    this._message = "Sending";
    this.httpService.postWithBody("Email/send", body)
      .pipe().subscribe((result: any) => {
        this._isWaitting = false;
        this._message = "";
        return result;
      },
        error => {
          this._isWaitting = false;
          console.log(error);
          return false;
        });
    return true;
  }
}

export class MailRequest {
    ToEmail!: string;
    Subject!: string;
    Body!: string;
    Attachments: any;
}