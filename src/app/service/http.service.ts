import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ConfigService } from './config.service';

@Injectable()
export class HttpService {
    constructor(private http: HttpClient, private config: ConfigService) {
    }

    public postExternalLink(path: string) : Observable<any>{
      return this.http.post<any>(path, this.getDefaultHeaders());
    }

    public post(serviceName: string, key: string) : Observable<any>{
        const path = this.config.getApiUrl(serviceName) + key;
        return this.http.post<any>(path, this.getDefaultHeaders());
    }

    public postWithBody(serviceName: string, body:any) : Observable<any>{
        const path = this.config.getApiUrl(serviceName);
        return this.http.post<any>(path, body);
    }

    public getExternalLink(path: string){
        return this.http.get(path, {responseType: 'text'});
    }

    public get(path: string){
      return this.http.get(path);
    }

    private getDefaultHeaders(): HttpHeaders {
        return new HttpHeaders();
    }
}
